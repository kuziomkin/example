
# Introduction
This document describes the requirements for QR-code money transfer service.

# Business requirements 

## Background
QR code payment is a contactless payment method where payment is performed by scanning a QR code from a mobile app.
According to Juniper Research, the number of QR code users is expected to grow to 2.2 billion in 2025. The US is expected to see a 240% increase in users from 2020 to 2025.

## Business opportunity
Business wants to launch a new feature that will help people to transfer money via QR-code. They assume that the contactless trend will extend, and Clients will use QR-code payments more often..

## Business goal
- 15 % of all payments would be performed via QR-code until the end of the 2022  year.

## Assumptions
- Number of QR code users is expected to grow to 2.2 billion in 2025.

## Scope of the project
![image](/out/Epics/QR/Diagrams/UseCase/QR_accountFunding_useCase.png)

**Description:**
1. Funding via QR-code:
- Create QR-code
- Display QR-code
- Share QR-code
2. Scan QR-code
- Display information about receiver
- Follow the link
4. Money transfer

# Solution Requirements
## Conceptual model
![image](/out/Epics/QR/Diagrams/AllOther/QR_ConceptModel.png)
**Description:**
- Payment microservice provides service to create QR-code, and perform payment transactions.
- Customer data service provides payment data to perform a transaction
- User Interface provides the interface to generate, share and display QR-code information
- The history service holds all history of transactions
- 3 party provider helps to perform payments
## Entity Data Model 
![image](/out/Epics/QR/Diagrams/Class/QR_entity_class.png)
## Information interaction flow
![image](/out/Epics/QR/Diagrams/Sequence/QR_create_sequence.png)